class Inmobiliaria {
    constructor() {
        this.pisos = [];
        this.casas = [];
        this.locales = [];
    }

    mostrarInmuebles() {
        const contenedor = document.getElementById("inmuebles")
        this.pisos.forEach(piso => {
            contenedor.innerHTML += `
            <div id = "inmueble">
                <h3>Piso</h3>
                <p>Referencia: ${piso.id}</p>
                <p>Direccion: ${piso.direccion}</p>
                <p>Superficie: ${piso.superficie}</p>
                <p>Precio: ${piso.calcularTotal()}</p>
                <p>Foto: ${piso.foto}</p>
                <p>Antiguedad: ${piso.antiguedad}</p>
                <p>Estado: ${piso.estado}</p>
                <p>Piso: ${piso.piso}</p>
                <p>Numero habitaciones: ${piso.numHabitaciones}</p>
                <p>Numero Baños: ${piso.numBanos}</p>
                <p>Tiene ascensor: ${piso.tieneAscensor}</p>
                <p>Superficie Terraza: ${piso.superficieTerraza}</p>
            </div>
            `
        });

        this.casas.forEach(casa => {
            contenedor.innerHTML += `
            <div id = "inmueble">
                <h3>Casa</h3>
                <p>Referencia: ${casa.id}</p>
                <p>Direccion: ${casa.direccion}</p>
                <p>Superficie: ${casa.superficie}</p>
                <p>Precio: ${casa.calcularTotal()}</p>
                <p>Foto: ${casa.foto}</p>
                <p>Antiguedad: ${casa.antiguedad}</p>
                <p>Estado: ${casa.estado}</p>
                <p>Tipo casa: ${casa.tipo}</p>
                <p>Numero habitaciones: ${casa.numHabitaciones}</p>
                <p>Numero Baños: ${casa.numBanos}</p>
                <p>M2 Jardin: ${casa.jardinMetrosCuadrados}</p>
                <p>Tiene piscina: ${casa.tienePiscina}</p>
            </div>
            `
        });

        this.locales.forEach(local => {
            contenedor.innerHTML += `
            <div id = "inmueble">
                <h3>Local</h3>
                <p>Referencia: ${local.id}</p>
                <p>Direccion: ${local.direccion}</p>
                <p>Superficie: ${local.superficie}</p>
                <p>Precio: ${local.calcularTotal()}</p>
                <p>Foto: ${local.foto}</p>
                <p>Antiguedad: ${local.antiguedad}</p>
                <p>Estado: ${local.estado}</p>
                <p>Ventanas: ${local.ventanas}</p>
                <p>Persiana Metalica: ${local.persianaMetal}</p>
                <p>Tipo local: ${local.tipo}</p>
                <p>Tipo suelo: ${local.tipoSuelo}</p>
            </div>
            `
        });
    }

    listasInmueble(inmueble) {
        if (inmueble instanceof Piso) {
            this.pisos.push(inmueble);
        } else if (inmueble instanceof Casa) {
            this.casas.push(inmueble);
        } else if (inmueble instanceof Local) {
            this.locales.push(inmueble);
        }

    }

    seleccionarInmueble() {
        const propertyTypeSelect = document.getElementById("property-type");
        const propertyForm = document.getElementById("property-form");

        propertyTypeSelect.addEventListener("change", function () {
            const propertyType = propertyTypeSelect.value;
            if (propertyType === "piso") {
                const piso = document.getElementById("piso").style.display = "block";
            } else if (propertyType === "casa") {
                const casa = document.getElementById("casa").style.display = "block";
            } else if (propertyType === "local") {
                const local = document.getElementById("local").style.display = "block";
            } else {
                propertyForm.innerHTML = "";
            }
        });
    }

    agregarInmueble() {
        const tipoInmueble = document.getElementById("property-type").value;
        if (tipoInmueble === "piso") {
            // Obtener los valores de los elementos de entrada
            let referencia = document.getElementById("id").value;
            let direccion = document.getElementById("address").value;
            let superficie = document.getElementById("m2").value;
            let precio = document.getElementById("precio").value;
            let foto = document.getElementById("foto").value;
            let antiguedad = document.getElementById("antiguedad").value;
            let estado = document.getElementById("estado").value;
            let piso = document.getElementById("pìso").value;
            let habitaciones = document.getElementById("hab").value;
            let baños = document.getElementById("baños").value;
            let ascensor = document.getElementById("ascensor").value;
            let terraza = document.getElementById("Terraza").value;

            const expresionRegular = /^[0-9]{7}[A-Z]{2}[0-9]{4}[A-Z]{4}[0-9]{2}$/;
            if (expresionRegular.test(referencia)) {
                return true;
            } else {
                alert("La referencia catastral se ha introducido mal!")
            }

            if (
                referencia == "" ||
                direccion == "" ||
                superficie == "" ||
                precio == "" ||
                foto == "" ||
                antiguedad == "" ||
                estado == "" ||
                piso == "" ||
                habitaciones == "" ||
                baños == "" ||
                ascensor == "" ||
                terraza == ""
              ) {
                alert("Por favor complete todos los campos del formulario.");
                return false;
              } else {
                // Si todos los campos están completos, podemos enviar el formulario
                const newPiso = new Piso(referencia, direccion, superficie, precio, foto, antiguedad, estado, piso, habitaciones, baños, ascensor, terraza);
                this.pisos.push(newPiso);
                return true;
              }

            
        } else if (tipoInmueble === "casa") {
            // Obtener los valores de los elementos de entrada
            let referencia = document.getElementById("id").value;
            let direccion = document.getElementById("address").value;
            let superficie = document.getElementById("m2").value;
            let precio = document.getElementById("precio").value;
            let foto = document.getElementById("foto").value;
            let antiguedad = document.getElementById("antiguedad").value;
            let estado = document.getElementById("estado").value;
            let tipo = document.getElementById("tipo").value;
            let habitaciones = document.getElementById("hab").value;
            let baños = document.getElementById("baños").value;
            let jardin = document.getElementById("jm2").value;
            let piscina = document.getElementById("piscina").value;

            const expresionRegular = /^[0-9]{7}[A-Z]{2}[0-9]{4}[A-Z]{4}[0-9]{2}$/;
            if (expresionRegular.test(referencia)) {
                return true;
            } else {
                alert("La referencia catastral se ha introducido mal!")
            }

            if (
                referencia == "" ||
                direccion == "" ||
                superficie == "" ||
                precio == "" ||
                foto == "" ||
                antiguedad == "" ||
                estado == "" ||
                tipo == "" ||
                habitaciones == "" ||
                baños == "" ||
                jardin == "" ||
                piscina == ""
              ) {
                alert("Por favor complete todos los campos del formulario.");
                return false;
              } else {
                // Si todos los campos están completos, podemos enviar el formulario
                const newCasa = new Casa(referencia, direccion, superficie, precio, foto, antiguedad, estado, tipo, habitaciones, baños, jardin, piscina);
                this.casas.push(newCasa);
                return true;
              }

            
        } else if (tipoInmueble === "local") {
            let referencia = document.getElementById("id").value;
            let address = document.getElementById("address").value;
            let m2 = document.getElementById("m2").value;
            let precio = document.getElementById("precio").value;
            let foto = document.getElementById("foto").value;
            let antiguedad = document.getElementById("antiguedad").value;
            let estado = document.getElementById("estado").value;
            let ventanas = document.getElementById("ventanas").value;
            let persiana = document.getElementById("persiana").value;
            let tipoLocal = document.getElementById("tipoLocal").value;
            let tipoSuelo = document.getElementById("tipoSuelo").value;

            const expresionRegular = /^[0-9]{7}[A-Z]{2}[0-9]{4}[A-Z]{4}[0-9]{2}$/;
            if (expresionRegular.test(referencia)) {
                return true;
            } else {
                alert("La referencia catastral se ha introducido mal!")
            }

            if (
                referencia == "" ||
                address == "" ||
                m2 == "" ||
                precio == "" ||
                foto == "" ||
                antiguedad == "" ||
                estado == "" ||
                ventanas == "" ||
                persiana == "" ||
                tipoLocal == "" ||
                tipoSuelo == ""
              ) {
                alert("Por favor complete todos los campos del formulario.");
                return false;
              } else {
                // Si todos los campos están completos, podemos enviar el formulario
                const newLocal = new Local(referencia, address, m2, precio, foto, antiguedad, estado, ventanas, persiana, tipoLocal, tipoSuelo);
                this.locales.push(newLocal);
                return true;
            }

            this.mostrarInmuebles();
            
        }
    }

    eliminarInmueble(indice) {
        this.inmuebles.splice(indice, 1);
    }
}

class Inmueble {
    constructor(id, direccion, superficie, precioBase, foto, antiguedad, estado) {
        this.id = id;
        this.direccion = direccion;
        this.superficie = superficie;
        this.precioBase = precioBase;
        this.foto = foto;
        this.antiguedad = antiguedad;
        this.estado = estado;
    }

    //Calcula el precio que se le quita a un inmueble segun el año de construccion
    calcularAntiguedad() {
        let date = new Date();
        let year = date.getFullYear();
        if (year - this.antiguedad > 20) {
            let porcentResto = 10;
            let precioFinal = (this.precioBase * (100 - porcentResto)) / 100;
            return precioFinal;
        } else if (year - this.antiguedad > 10 && year - this.antiguedad < 20) {
            let porcentResto = year - this.antiguedad - 10;
            let precioFinal = (this.precioBase * (100 - porcentResto)) / 100;
            return precioFinal;
        } else {
            return this.precioBase;
        }
    }

    //Devuelve el calculo anteriro para poder utilizarlo en todas las clases que tien como herencia.
    calcularTotal() {
        return this.calcularAntiguedad();
    }
}

class Piso extends Inmueble {
    constructor(id, direccion, superficie, precioBase, foto, antiguedad, estado, piso, numHabitaciones, numBanos, tieneAscensor, superficieTerraza) {
        super(id, direccion, superficie, precioBase, foto, antiguedad, estado);
        this.piso = piso;
        this.numHabitaciones = numHabitaciones;
        this.numBanos = numBanos;
        this.tieneAscensor = tieneAscensor;
        this.superficieTerraza = superficieTerraza;
    }

    //Calculo de precio que se le añade segun si esta en la tercera planta y tiene ascensor
    calcularAscensor() {
        if (this.piso >= 3 && this.tieneAscensor) {
            let precioFinal = ((this.precioBase * 103) / 100) - this.precioBase;
            return precioFinal;
        } else {
            return 0;
        }
    }

    //Calcula el precio que se le añade segun la superficie de la terraza.
    calcularTerraza() {
        if (this.superficieTerraza > 0) {
            let precioTerraza = 300 * this.superficieTerraza;
            return precioTerraza;
        } else {
            return 0;
        }
    }

    //Calculo total del precio de un piso
    calcularTotal() {
        return super.calcularTotal() + this.calcularTerraza() + this.calcularAscensor();
    }
}

class Casa extends Inmueble {
    constructor(id, direccion, superficie, precioBase, foto, antiguedad, estado, tipo, numHabitaciones, numBanos, jardinMetrosCuadrados, tienePiscina) {
        super(id, direccion, superficie, precioBase, foto, antiguedad, estado);
        this.tipo = tipo;
        this.numHabitaciones = numHabitaciones;
        this.numBanos = numBanos;
        this.jardinMetrosCuadrados = jardinMetrosCuadrados;
        this.tienePiscina = tienePiscina;
    }

    //Calculo del precio que se le añade segun la superficie del jardin.
    calcularJardin() {
        if (this.jardinMetrosCuadrados > 0) {
            if (this.jardinMetrosCuadrados > 250) {
                return ((this.precioBase * 109) / 100) - this.precioBase;
            } else if (this.jardinMetrosCuadrados > 100) {
                return ((this.precioBase * 105) / 100) - this.precioBase;
            } else {
                return 0;
            }
        }
    }

    //Calculo del precioque se le añade a una casa si tiene piscina
    calcularPiscina() {
        if (this.tienePiscina == true) {
            return ((this.precioBase * 104) / 100) - this.precioBase;
        } else {
            return 0;
        }
    }

    //Calcula el precio total de una casa
    calcularTotal() {
        return super.calcularTotal() + this.calcularJardin() + this.calcularPiscina();
    }
}

class Local extends Inmueble {
    constructor(id, direccion, superficie, precioBase, foto, antiguedad, estado, ventanas, persianaMetal, tipo, suelo) {
        super(id, direccion, superficie, precioBase, foto, antiguedad, estado)
        this.ventanas = ventanas;
        this.persianaMetal = persianaMetal;
        this.tipo = tipo;
        this.suelo = suelo;
    }

    //Calcula el precio que se le añade en funcion de la supreficie que tenga un local
    calcularSuperficie() {
        if (this.superficie > 50) {
            return ((this.precioBase * 101) / 100) - this.precioBase;
        } else {
            return 0;
        }
    }

    //Calcula el precio que se le añado o desgraba al precio segun las ventanas
    calcularVentanas() {
        if (this.ventanas > 4) {
            return ((this.precioBase * 104) / 100) - this.precioBase;
        } else if (this.ventanas <= 1) {
            return ((this.precioBase * 98) / 100);
        } else {
            return 0;
        }
    }

    //Calcula el precio total de un local
    calcularTotal() {
        let precioSinIVA = super.calcularTotal() + this.calcularSuperficie() + this.calcularVentanas();
        let precioConIVA = (precioSinIVA * 125) / 100;
        if (this.tipo == "industrial" && this.suelo == "urbano") {
            return precioConIVA;
        } else {
            return precioSinIVA;
        }
    }
}


const inmobiliaria = new Inmobiliaria();

const piso = new Piso(1, "Joan 10 2o 1a", 150, 150000, "www.foto.com", 2020, "reformado",  4, 3, 2, true, 4);
inmobiliaria.listasInmueble(piso);

const casa = new Casa(2, "Salvador 30, 4o 1a", 300, 400000, "www.adrifeo.com", 1, "nueva", "adosada", 5, 3, 140, true);
inmobiliaria.listasInmueble(casa);

const local = new Local(3, "Francesc 20 3o 5a", 51, 60000, "www.adrilesionao.com", 21, "segunda mano", 5, true, "industrial", "urbano");
inmobiliaria.listasInmueble(local);

const piso2 = new Piso(4, "Calle Gran Via 30", 90, 100000, "www.foto2.com", 5, "buen estado", 3, 2, 1, false, 0);
inmobiliaria.listasInmueble(piso2);

const casa2 = new Casa(5, "Avenida del Parque, 15", 300, 350000, "www.foto3.com", 10, "nuevo", "adosada", 5, 3, 200, true);
inmobiliaria.listasInmueble(casa2);

const local2 = new Local(6, "Calle Mayor, 45", 80, 200000, "www.foto4.com", 20, "a reformar", 2, true, "comercial", "urbano");
inmobiliaria.listasInmueble(local2);


inmobiliaria.mostrarInmuebles();
inmobiliaria.seleccionarInmueble();